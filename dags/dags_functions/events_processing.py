import redis
import dags_functions.extra.configs.enviroments as env
from dags_functions.extra.files_processing import get_files_list_via_boto, download_via_http
from dags_functions.extra.kafka_utils import get_producer, transfer_lines_to_kafka
from dags_functions.extra.ch_utils import get_ch_client, clean_table


def transfer_data_to_kafka():

    """
    Подгрузка списка файлов и отправка в нужный топик тех файлов,
    которые еще не загружены.

    Args:

    Returns:
    
    """

    # инициализация продюсера
    producer = get_producer(f"{env.MAIN_HOST}:{env.BROKER_PORT}")

    # инициализация клиента Redis
    r = redis.Redis(host=env.MAIN_HOST, port=env.REDIS_PORT, decode_responses=True)

    # подгружаем список файлов
    files_paths = get_files_list_via_boto(bucket_name=env.S3_BUCKET,
                                          endpoint_url=env.S3_ENDPOINT,
                                          access_key=env.S3_KEY_ID,
                                          secret_key=env.S3_SECRET_KEY)

    # проходим по всем файлам и отправляем в кафку в нужный бакет
    for path in files_paths:
        # проверяем, что еще не писали
        if not r.get(path):
            
            # грузим данные в память
            data = download_via_http(path)
            
            # выбираем нужный топик
            for topic in env.TOPICS_LIST:
                if topic in path:
                    topic_dist = topic

            # отправляем в кафку
            try:
                transfer_lines_to_kafka(producer, topic_dist, data)
            except NameError:
                print(f'Название файла {path} не матчится со списком топиков. Не буду записывать(')
                continue

            # пишем в Redis путь к файлу, чтобы больше не перезаписывать
            r.set(path, 'done')

            print(f"{path} в отправлен в Kafka!")
            
    return


def clear_data_in_redis_and_ch():

    """
    Очищает данные в Redise и в Clickhouse для полной
    перезаписи данных.

    Args:

    Returns:
    
    """

    # инициализируем Redis
    r = redis.Redis(host=env.MAIN_HOST, port=env.REDIS_PORT)

    # инициализируем клиент CH
    ch_client = get_ch_client(host=env.MAIN_HOST, port=env.CH_PORT)

    # таблицы имеют те же названия, что и топики в кафке
    for table_name in env.TOPICS_LIST:
        # очищаем
        clean_table(ch_client, table_name, env.CH_DB)

    # очищаем Redis
    r.flushall()

    print('Всё почищено!')

    return 
