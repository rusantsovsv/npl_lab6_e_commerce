"""
Утилиты для работы с CH
"""

import clickhouse_connect


def get_ch_client(host: str, port: str):

    """
    Получение клиента ClickHouse по переданному хосту
    и порту.

    Args:
        host: хост, на котором развернут ClickHouse.
        port: tcp порт.

    Returns:
        инициализированный клиент.
    """

    return clickhouse_connect.get_client(host=host, port=port)


def clean_table(client, table_name: str, db_name: str):

    """
    Очистка таблицы по переданному имени.

    Args:

    Returns:
    
    """

    client.command(f'truncate table {db_name}.{table_name}')

    return
    