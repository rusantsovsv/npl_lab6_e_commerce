"""
Утилиты для работы с Kafka
"""

from confluent_kafka import Producer


def delivery_callback(err, msg):

    """
    Служебные сообщения при отправке данных

    Args:
        err: тип ошибки.
        msg: сообщение ошибки из кафки.

    Returns:
        печатет сообщение об успешной или неудачной отправке.
    """
    
    if err:
        print('%% Message failed delivery: %s\n' % err)
    else:
        ...
        # print('%% Message delivered to %s [%d] @ %d\n' % (msg.topic(), msg.partition(), msg.offset()))


def get_producer(kafka_address: str):

    """
    Возвращает настроенный клиент продюсера.

    Args:
        kafka_address: адрес кафки в формате host:port

    Returns:
    
    """

    # создаем конфигурацию продюсера
    conf = {
        'bootstrap.servers': kafka_address,
    }

    # возвращаем объект продюсера
    return Producer(**conf)


def transfer_lines_to_kafka(producer, topic: str, lines: list) -> None:

    """
    Отправка подготовленных строк в Kafka.

    Args:
        producer: объект продюсера.
        topic: топик, в который льем данные.
        lines: список строк для записи в Kafka.        

    Returns:
    
    """

    for line in lines:
        try:
            producer.produce(topic, line, callback=delivery_callback)
            producer.poll(0)
        except KeyboardInterrupt:
            break

    producer.flush()

    return
    