"""
Подгрузка переменных окружения из .env
"""

import os
from dotenv import load_dotenv

# подгружаем переменные из .env (смотри .env.example)
load_dotenv(os.path.join(os.path.dirname(__file__), '.env'))

MAIN_HOST = os.environ['MAIN_HOST']
BROKER_PORT = os.environ['BROKER_PORT']
REDIS_PORT = os.environ['REDIS_PORT']

# для доступа через boto3
S3_ENDPOINT = os.environ['S3_ENDPOINT']
S3_BUCKET = os.environ['S3_BUCKET']
S3_KEY_ID = os.environ['S3_KEY_ID']
S3_SECRET_KEY = os.environ['S3_SECRET_KEY']

# соберем все топики в один список
TOPICS_LIST = [os.environ[env] for env in os.environ if 'TOPIC_' in env]

# порт Clickhouse
CH_PORT = os.environ['CH_PORT']

CH_DB = os.environ['CH_DB']
