"""
Утилиты для загрузки файлов 
"""

import io
import re
import boto3
import zipfile
import requests


def get_files_list_via_boto(bucket_name: str, endpoint_url: str, access_key: str, secret_key: str):

    """
    Получение списка файлов через S3 клиент.

    Args:
        bucket_name: имя бакета, в который нужно сходить.
        endpoint_url: эндпойнт YandexCloud.
        access_key: ключ доступа.
        secret_key: секретный ключ

    Return:
        список файлов
    """

    session = boto3.session.Session()

    s3_client = session.client(
        service_name='s3',
        endpoint_url=endpoint_url,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
    )

    # создаем пагинатор, чтобы получить > 1000 имен
    paginator = s3_client.get_paginator('list_objects_v2')
    pages = paginator.paginate(Bucket=bucket_name)

    for page in pages:
        for obj in page['Contents']:
            # формируем полный путь к файлу
            path = endpoint_url.replace('https://', f'https://{bucket_name}.') + obj['Key']
            yield path


def get_files_list_via_http(bucket_address: str) -> list:

    """
    Получение списка файлов в бакете через
    парсинг html страницы бакета.

    Args:
        bucket_address: адрес публичного бакета с данными.
 
    Returns:
        список путей к файлам в бакете.
    """

    content = requests.get(bucket_address)

    files_paths = re.findall('<Key>(.+?)</Key>', content.content.decode('utf-8'))

    return [f"{bucket_address}{f}" for f in files_paths] 


def download_via_http(full_path: str) -> list:

    """
    Загрузка jsonl через http. 

    Args:
        full_path: полный путь к файлу в бакете (адрес бакета + имя файла).

    Returns:
        список строк из jsonl, готовых к трансферу.
    """

    # качаем файл
    obj = requests.get(full_path)

    # распаковываем в байты
    zip_data = zipfile.ZipFile(io.BytesIO(obj.content), "r")

    # читаем из байтов в строки
    lines = zip_data.read(zip_data.infolist()[0]).decode()

    # немного почистим
    lines = [l.replace('\\', '').strip() for l in lines.splitlines()]

    return lines
