import airflow
from airflow import DAG
from airflow.operators.python import PythonOperator

from dags_functions.events_processing import clear_data_in_redis_and_ch

default_args = {'owner': 'sv.rusantsov',
                'start_date': airflow.utils.dates.days_ago(1)}

params_of_dag = DAG('clear_data_by_trigger',
                    tags=['lab_06'],
                    default_args=default_args,
                    schedule_interval=None)

with params_of_dag as dag:

    clear_data_in_redis_and_ch = PythonOperator(task_id='clear_all',
                                                python_callable=clear_data_in_redis_and_ch)
