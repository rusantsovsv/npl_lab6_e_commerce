import datetime
from airflow import DAG
from airflow.operators.python import PythonOperator

from dags_functions.events_processing import transfer_data_to_kafka

default_args = {'owner': 'sv.rusantsov',
                # 'start_date': airflow.utils.dates.days_ago(2),
                'start_date': str(datetime.date(2024, 4, 19)),
                'depends_on_past': False
                }

params_of_dag = DAG('load_events',
                    tags=['lab_06'],
                    catchup=False,
                    default_args=default_args,
                    schedule_interval='5 0/1 * * *')  # каждый час в 5 минут

with params_of_dag as dag:

    transfer_data_to_kafka = PythonOperator(task_id='transfer_data_to_kafka',
                                            python_callable=transfer_data_to_kafka)
