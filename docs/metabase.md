# Установка коннектора ClickHouse

Чтобы установить драйвер Clickhouse нужно его сначала скачать. 
Подробности в [документации к драйверу](https://github.com/ClickHouse/metabase-clickhouse-driver?tab=readme-ov-file).

Запустите скрипт [load_ch_driver_for_metabase.sh](https://gitlab.com/rusantsovsv/npl_lab6_e_commerce/-/blob/main/load_ch_driver_for_metabase.sh?ref_type=heads) чтобы скачать нужную версию.


После скачивания нужно переподнять контейнер. В админке должен появиться драйвер:
![img.png](pic/metabase_ch_driver.png)

## Запросы

Так как у меня в распоряжении 2 ноды - делаем запросы на кластере. Пример:

```shell
select
	utm_source,
	count(event_type) as count_buy
from cluster('cluster1', final_lab.browser_events)  be 
join cluster('cluster1', final_lab.location_events) le on le.event_id = be.event_id
group by utm_source
```

Для такой возможности нужно создать пользователя на кластере с нужными правами доступа. Пример для пользователя metabase:

```shell
-- создание пользователя
CREATE USER metabase IDENTIFIED WITH plaintext_password BY 'password_six_stars' on CLUSTER cluster1;
-- права на селекты в нужную базу
GRANT SELECT on final_lab.* to metabase on CLUSTER cluster1;
-- права на запросы к кластеру
GRANT CREATE TEMPORARY TABLE, REMOTE ON *.* to metabase on CLUSTER cluster1;
```