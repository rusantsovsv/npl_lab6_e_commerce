# Структура таблиц

Мы работаем с Kafka, поэтому для каждой таблицы создадим MV для трансфера данных. Но для начала создаем БД на кластере:

```shell
CREATE DATABASE final_lab ON CLUSTER 'cluster1';
```

Попробуем задействовать обе ноды для обработки запросов.

## Таблица browser_events

Таблица с данными, которые получим из Кафки:

```shell
    CREATE TABLE final_lab.browser_events ON CLUSTER 'cluster1'
    (
        event_timestamp DateTime64,
        event_id String,
        event_type LowCardinality(String),
        click_id String,
        browser_name LowCardinality(String),
        browser_user_agent String,
        browser_language LowCardinality(String)
    ) ENGINE = MergeTree()
      PARTITION BY toYYYYMMDD(event_timestamp)
      ORDER BY (event_timestamp, event_id, click_id);
```

Таблица для вычитывания данных из Кафки:

```shell
  CREATE TABLE final_lab.browser_events_from_kafka ON CLUSTER 'cluster1'
    (
        event_timestamp DateTime64,
        event_id String,
        event_type LowCardinality(String),
        click_id String,
        browser_name LowCardinality(String),
        browser_user_agent String,
        browser_language LowCardinality(String)
    ) ENGINE = Kafka 
      SETTINGS kafka_broker_list = '{KAFKA_SERVER_IP}:{PORT}',
               kafka_topic_list = '{browser_events_topic}',
               kafka_group_name = '{browser_events_topic}_group_1',
               kafka_format = 'JSONEachRow';
```

MV для перелива данных:

```shell
    CREATE MATERIALIZED VIEW final_lab.browser_events_mv ON CLUSTER 'cluster1' TO final_lab.browser_events AS
    SELECT *
    FROM final_lab.browser_events_from_kafka;
```

Для остальных видов данных создаем аналогичные таблицы, только делаем их ReplacingMergeTree для фильтрации дублей.

## Таблица device_events

```shell

    -- основная таблица
    CREATE TABLE final_lab.device_events ON CLUSTER 'cluster1'
    (
        click_id String,
        os LowCardinality(String),
        os_name LowCardinality(String),
        os_timezone LowCardinality(String),
        device_type LowCardinality(String),
        device_is_mobile Bool,
        user_custom_id String,
        user_domain_id String
    ) ENGINE = ReplacingMergeTree()
      PARTITION BY device_type
      PRIMARY KEY click_id;
      
    -- для данных из Kafka
    CREATE TABLE final_lab.device_events_from_kafka ON CLUSTER 'cluster1'
    (
        click_id String,
        os LowCardinality(String),
        os_name LowCardinality(String),
        os_timezone LowCardinality(String),
        device_type LowCardinality(String),
        device_is_mobile Bool,
        user_custom_id String,
        user_domain_id String
    ) ENGINE = Kafka 
      SETTINGS kafka_broker_list = '{KAFKA_SERVER_IP}:{PORT}',
               kafka_topic_list = '{device_events_topic}',
               kafka_group_name = '{device_events_topic}_group_1',
               kafka_format = 'JSONEachRow';
               
    --mv для перелива
    CREATE MATERIALIZED VIEW final_lab.device_events_mv ON CLUSTER 'cluster1' TO final_lab.device_events AS
    SELECT *
    FROM final_lab.device_events_from_kafka;
```


## Таблица geo_events

```shell
    -- основная таблица
    CREATE TABLE final_lab.geo_events ON CLUSTER 'cluster1'
    (
        click_id String,
        geo_latitude Float,
        geo_longitude Float,
        geo_country LowCardinality(String),
        geo_timezone LowCardinality(String),
        geo_region_name LowCardinality(String),
        ip_address String
    ) ENGINE = ReplacingMergeTree()
      PARTITION BY geo_country
      PRIMARY KEY click_id;
      
    -- для данных из Kafka
    CREATE TABLE final_lab.geo_events_from_kafka ON CLUSTER 'cluster1'
    (
        click_id String,
        geo_latitude Float,
        geo_longitude Float,
        geo_country LowCardinality(String),
        geo_timezone LowCardinality(String),
        geo_region_name LowCardinality(String),
        ip_address String
    ) ENGINE = Kafka 
      SETTINGS kafka_broker_list = '{KAFKA_SERVER_IP}:{PORT}',
               kafka_topic_list = '{geo_events_topic}',
               kafka_group_name = '{geo_events_topic}_group_1',
               kafka_format = 'JSONEachRow';
               
    --mv для перелива
    CREATE MATERIALIZED VIEW final_lab.geo_events_mv ON CLUSTER 'cluster1' TO final_lab.geo_events AS
    SELECT *
    FROM final_lab.geo_events_from_kafka;
```


## Таблица location_events

```shell
    -- основная таблица
    CREATE TABLE final_lab.location_events ON CLUSTER 'cluster1'
    (
        event_id String,
        page_url LowCardinality(String),
        page_url_path LowCardinality(String),
        referer_url LowCardinality(String),
        referer_medium LowCardinality(String),
        utm_medium LowCardinality(String),
        utm_source LowCardinality(String),
        utm_content LowCardinality(String),
        utm_campaign LowCardinality(String)
    ) ENGINE = ReplacingMergeTree()
      PARTITION BY page_url_path
      PRIMARY KEY event_id;
      
    -- для данных из Kafka
    CREATE TABLE final_lab.location_events_from_kafka ON CLUSTER 'cluster1'
    (
        event_id String,
        page_url LowCardinality(String),
        page_url_path LowCardinality(String),
        referer_url LowCardinality(String),
        referer_medium LowCardinality(String),
        utm_medium LowCardinality(String),
        utm_source LowCardinality(String),
        utm_content LowCardinality(String),
        utm_campaign LowCardinality(String)
    ) ENGINE = Kafka 
      SETTINGS kafka_broker_list = '{KAFKA_SERVER_IP}:{PORT}',
               kafka_topic_list = '{location_events_topic}',
               kafka_group_name = '{location_events_topic}_group_1',
               kafka_format = 'JSONEachRow';
               
    --mv для перелива
    CREATE MATERIALIZED VIEW final_lab.location_events_mv ON CLUSTER 'cluster1' TO final_lab.location_events AS
    SELECT *
    FROM final_lab.location_events_from_kafka;
```