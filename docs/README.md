# Дополнительная информация по настройке и работе с сервисами

[Airflow](https://gitlab.com/rusantsovsv/npl_lab6_e_commerce/-/blob/main/docs/airflow.md?ref_type=heads) - необходимые для работы даги.

[Kafka](https://gitlab.com/rusantsovsv/npl_lab6_e_commerce/-/blob/main/docs/kafka.md?ref_type=heads) - информация по созданию и удалению топиков, просмотра списка топиков, создания продюсера и консюмера.

[Clickhouse](https://gitlab.com/rusantsovsv/npl_lab6_e_commerce/-/blob/main/docs/clickhouse.md?ref_type=heads) - создание основных таблиц и материализованных представлений для перелива данных из Kafka.

[Metabase](https://gitlab.com/rusantsovsv/npl_lab6_e_commerce/-/blob/main/docs/metabase.md?ref_type=heads) - получение драйвера для подключения к Clickhouse. Основные дашборды