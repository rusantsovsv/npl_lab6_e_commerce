# Kafka

Для работы настраиваем 4 топика для каждого типа данных в файлах, в каждом сделаем retantion retention.ms=300000 (при необходимости можно уменьшить)

## Создание топика

На виртуалке, где развернута Kafka нужно запустить команду:

```shell
docker exec -it broker \
      kafka-topics --create \
      --bootstrap-server broker:9092 \
      --config retention.ms=300000 \
      --topic <<тут_название_топика>>
```

## Создание консьюмера и продюсера

Создание консьюмера

```shell
docker exec -it broker \
      kafka-console-consumer \
      --bootstrap-server broker:9092 \
      --group <<тут_номер_группы>>> \
      --topic <<тут_название_топика>>
```

Создание продюсера

```shell
docker exec -it broker \
      kafka-console-producer \
      --bootstrap-server broker:9092 \
	  --topic <<тут_название_топика>>
```

## Дополнительные полезности

Посмотреть список созданных топиков:

```shell
docker exec -it broker \
      kafka-topics --bootstrap-server broker:9092 --list
```

Подробное описание топиков:

```shell
docker exec -it broker \
      kafka-topics --bootstrap-server broker:9092 --describe
```

Удалить топик:

```shell
docker exec -it broker \
      kafka-topics --delete \
      --bootstrap-server broker:9092 \
      --topic <<<тут_название_топика>>>
```

